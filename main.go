package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"gopkg.in/robfig/cron.v2"
)

var payload []map[string]interface{}

var ODP_DS_URL = getEnv("ODP_DS_URL")
var FREQ = getEnv("FREQ")
var RESET_COUNTER = getEnv("RESET_COUNTER")

func main() {
	c := cron.New()
	c.AddFunc(FREQ, func() {
		log.Print("[ INFO ] ::: Cron started :::", time.Now())

		client := &http.Client{}

		getSeqMaster()
		payload[0]["hdfcSeqNum"] = RESET_COUNTER
		log.Print("[ INFO ] PAYLOAD ", payload)

		requestBody, err := json.Marshal(payload[0])
		if err != nil {
			log.Fatal(err)
		}

		id := payload[0]["_id"].(string)
		// log.Print("ID: ", id, requestBody)
		req, _err := http.NewRequest("PUT", ODP_DS_URL+"/"+id, bytes.NewBuffer(requestBody))
		if _err != nil {
			log.Fatal(_err)
		}
		req.Header.Add("Content-Type", "application/json")
		resp, _err := client.Do(req)
		if _err != nil {
			log.Fatal(_err)
		}
		Body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		log.Printf("[ INFO ] PUT RESPONSE: %s", Body)
	})

	c.Start()
	defer c.Stop() // Stop the scheduler (does not stop any jobs already running).
	for {
	}
	// Added time to see output
	// time.Sleep(10 * time.Minute)

}

func getSeqMaster() {
	resp, _err := http.Get(ODP_DS_URL)
	if _err != nil {
		log.Fatal(_err)
	}
	Body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("[ INFO ] GET RESPONSE: %s", Body)

	_error := json.Unmarshal(Body, &payload)
	if _error != nil {
		log.Fatal(err)
	}
	// log.Print("[ INFO ] Unmarshalled payload: ", payload)
}

func getEnv(key string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return ""
}